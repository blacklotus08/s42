const mongoose = require("mongoose");
const userSchema = new mongoose.Schema ({
	email: {
		type: String,
		required : [true, "Email is required"]
	},
	password: {
		type: String,
		required : [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProducts: [
	{
		products: [{
			productId: {
				type: String,
				required: [true, "productId is required"]
			},
			productName: {
				type: String,
				required: [true, "productName is required"]
			},
			quantity: {
				type: Number,
				required: [true, "quantity is required"]
			}
		}],
		totalAmount: {
			type: Number,
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}
	}
	]
})

module.exports = mongoose.model("User", userSchema)