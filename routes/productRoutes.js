const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers.js");
const auth = require("../auth.js");

// Adding Products
router.post("/create", (req, res) => {
    const data = {
      product: req.body,
    //   isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

	productController
    .addProduct(data.product)
    .then((resultFromController) => res.send(resultFromController));

  //   if (data.isAdmin) {
  //     productController
  //       .addProduct(data.product)
  //       .then((resultFromController) => res.send(resultFromController));
  //   } else {
  //     res.send(false);
  //   }
});

// Retrieving ALL Products
router.get("/allProducts", (req, res) => {
  productController
    .allProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieving ALL Active Products
router.get("/activeProducts", (req, res) => {
  productController
    .activeProducts()
    .then((resultFromController) => res.send(resultFromController));
});

// Retrieving a Single Product
router.get("/:productId", (req, res) => {
  productController
    .specificProduct(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

// Updating a Product Info (Admin Only)
router.put("/update/:productId", auth.verify, (req, res) => {
  productController
    .updateProduct(data.product, data.params)
    .then((resultFromController) => res.send(resultFromController));

  // const data = {
  // 	product: req.body,
  // 	isAdmin: auth.decode(req.headers.authorization).isAdmin,
  // 	params: req.params
  // }

  // if(data.isAdmin) {

  // 	productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController));
  // } else {

  // 	res.send(false)
  // }
});

// Archive Product (Admin Only)

router.put("/archive/:productId", auth.verify, (req, res) => {
  productController
    .archiveProduct(data.params)
    .then((resultFromController) => res.send(resultFromController));
  // const data = {
  // 	isAdmin: auth.decode(req.headers.authorization).isAdmin,
  // 	params: req.params
  // }

  // if(data.isAdmin) {

  // 	productController.archiveProduct(data.params).then(resultFromController => res.send(resultFromController));
  // } else {

  // 	res.send(false)
  // }
});

// Activating Product (Admin Only)

router.put("/activate/:productId", auth.verify, (req, res) => {
  productController
    .activateProduct(data.params)
    .then((resultFromController) => res.send(resultFromController));

  // const data = {
  // 	isAdmin: auth.decode(req.headers.authorization).isAdmin,
  // 	params: req.params
  // }

  // if(data.isAdmin) {

  // 	productController.activateProduct(data.params).then(resultFromController => res.send(resultFromController));
  // } else {

  // 	res.send(false)
  // }
});

router.get("/allOrders", (req, res) => {
  productController
    .allOrders()
    .then((resultFromController) => res.send(resultFromController));

  // const data = {
  // 	isAdmin: auth.decode(req.headers.authorization).isAdmin
  // }

  // if(data.isAdmin) {

  // 	productController.allOrders().then(resultFromController => res.send(resultFromController))
  // } else {

  // 	res.send(false)
  // }
});

module.exports = router;
