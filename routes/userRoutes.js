const express = require("express")
const router = express.Router();
const userController = require("../controllers/userControllers.js")
const auth = require("../auth.js")

// User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => {res.send(resultFromController)})

})

// User Authentication

router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => {res.send(resultFromController)})
})

// Non-admin user add to cart

router.post("/addToCart", auth.verify, (req,res) => {

	const data ={
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {
		res.send(false)
	} else {userController.userCart(data).then(resultFromController => {res.send(resultFromController)})
}
	
})

// Retrieve all user
router.get("/allUsers", (req, res) => {

	userController.allUsers().then(resultFromController => res.send(resultFromController));
})

// Retrieve User details
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});


// Set user as Admin

router.put("/access/:userId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {

		userController.userAccess(data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false)
	}

})

/*
router.get("/orders/:userId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: req.params
	}

	if(data.isAdmin) {

		userController.userOrders(data.params).then(resultFromController => res.send(resultFromController));
	} else {

		res.send(false)
	}
})
*/



module.exports = router;
