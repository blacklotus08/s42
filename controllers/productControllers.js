const Product = require("../models/Product.js");
const auth = require("../auth.js")

// Adding Products
module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name: product.name,
		description: product.description,
		price: product.price
	});

	return newProduct.save().then((product, err) => {

		if(err) {

				return false

			} else {

				return true;
			}
	})
}

// Retrieving ALL Products
module.exports.allProducts = () => {

	return Product.find({}).then(result => {

		return result;
	})
}

// Retrieving ALL Active Products
module.exports.activeProducts = () => {

	return Product.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieving a Single Product
module.exports.specificProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		return result;
	})
}

// Updating a Product Info (Admin Only)
module.exports.updateProduct = (product, paramsId) => {

	let updatedProduct = {
		name: product.name,
		description: product.description,
		price: product.price
	}

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}



// Archiving Product (Admin Only)

module.exports.archiveProduct = (reqParams) => {

	let updateActiveField = {
		isActive: false
	}

	// 
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}

// Activate Product (Admin Only)

module.exports.activateProduct = (reqParams) => {

	let updateActiveField = {
		isActive: true
	}

	// 
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}

module.exports.allOrders = () => {

	return Product.find({userOrders}).then(result => {
		return result
	})
}





















