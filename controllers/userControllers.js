const User = require("../models/User.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js")
const Product = require("../models/Product.js")

// User Registration
module.exports.registerUser = (requestBody) => {

	let newUser = new User ({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false

		} else {

			return true;
		}
	})
}

// User Authentication
module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {
			
			return false;
		
		} else {

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			if(isPasswordCorrect) {

				return {access: auth.createAccessToken(result)}

			} else {

				return false;
			}
		}
	})

}

module.exports.userCart = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderedProducts.push({productId: data.productId});

		return user.save().then((added, err) => {

			if(err) {
				return false;
			} else {
				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId: data.userId});

		return product.save().then((added, err) => {

			if(err) {

				return false;
			} else {

				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {

		return true
	} else {

		return false
	}
}
// All User Details
module.exports.allUsers = () => {

	return User.find({}).then(result => {

		return result;
	})
}

// User details (Specific)

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		result.password = "";

		return result;

	});

};

module.exports.userAccess = (reqParams) => {

	let updateActiveField = {
		isAdmin: true
	}

	// 
	return User.findByIdAndUpdate(reqParams.userId, updateActiveField).then((result, err) => {

		if(err) {

			return false;

		} else {

			return true;
		}
	})
}

/*

module.exports.userOrders = (reqParams) => {

	return User.findById()
}
*/






