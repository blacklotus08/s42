//dependencies
const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");


//Server
const app = express();

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://admin:admin1234@b256tavas.qyzjlfy.mongodb.net/B256_EcommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


// Routes
app.use(cors());
app.use("/users", userRoutes);
app.use("/products", productRoutes);


let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Were connected to the cloud database`));

app.listen(4000, () => console.log (`API is now online on port 4000`));